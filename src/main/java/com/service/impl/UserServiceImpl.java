package com.service.impl;

import com.dao.UserDao;
import com.pojo.User;
import com.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Houxudong
 **/
@Service
public class UserServiceImpl implements UserService{
    @Autowired
    private UserDao userDao;
    @Override
    public void doRegist(User user) {
        userDao.addUser(user);
    }

    @Override
    public User getUserByName(String userName) {
        return userDao.getUserByName(userName);
    }

    @Override
    public User doLogin(User user) {
        User newUser = userDao.getUserByName(user.getUserName());
        System.out.println("xxx");
        if(user.getPassword().equals(newUser.getPassword())){
            return newUser;
        }
        return null;
    }

    @Override
    public void doUpdateUser(User user) {
        userDao.uploadUser(user);
    }

    @Override
    public User getUserNById(Integer userId) {
        return userDao.getUserById(userId);
    }
}
