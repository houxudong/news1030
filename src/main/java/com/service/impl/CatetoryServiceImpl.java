package com.service.impl;

import com.dao.CatetoryDao;
import com.pojo.Catetory;
import com.service.CatetoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Houxudong
 **/
@Service
public class CatetoryServiceImpl implements CatetoryService{
    @Autowired
    private CatetoryDao catetoryDao;
    @Override
    public List<Catetory> getAllCatetory() {
        return catetoryDao.getAllCatetory();
    }
}
