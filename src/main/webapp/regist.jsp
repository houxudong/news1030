<%@ page pageEncoding="UTF-8" %>
<html>
<head>
    <title>用户注册</title>
    <script type="text/javascript" src="js/jquery-1.10.1.min.js"></script>
    <style type="text/css">
        #msg{
            color: red;
        }
    </style>
</head>
<body>
    <form action="/user/doRegist.html">
        用户名:<input type="text" id="userName" name="userName" onblur="check('name')"><span id="msg"></span><br>
        密码:<input type="password" name="password" id="pwd1"><br>
        确认密码:<input type="password" name="repassword" id="pwd2"><span id="msg2"></span><br>
        <input type="button" value="注册" onclick="check('btn')"><br>
    </form>

</body>
<script type="text/javascript">
    function check(obj) {
        var pwd1 = $("#pwd1").val();
        var pwd2 = $("#pwd2").val();
        $.ajax({
            url:"/user/check.html",
            data:"userName="+$("#userName").val(),
            success:function (data) {
                $("#msg").html(data.msg);
                if(data.status==true&&obj=='btn'){
                    if(pwd1!=pwd2){
                        $("#msg2").html('两次输入密码不一致');
                        return;
                    }
                    $("form").submit();
                }
            }
        });
    }
</script>
</html>
