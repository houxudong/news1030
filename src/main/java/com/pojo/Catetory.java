package com.pojo;

/**
 * @Houxudong
 **/
public class Catetory {
    private Integer catetoryId;
    private String catetoryName;

    public Integer getCatetoryId() {
        return catetoryId;
    }

    public void setCatetoryId(Integer catetoryId) {
        this.catetoryId = catetoryId;
    }

    public String getCatetoryName() {
        return catetoryName;
    }

    public void setCatetoryName(String catetoryName) {
        this.catetoryName = catetoryName;
    }
}
