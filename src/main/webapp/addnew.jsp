<%@ page pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>发布新闻</title>
    <link href="/js/kindeditor-4.1.10/themes/default/default.css" type="text/css" rel="stylesheet">
    <script src="/js/jquery-1.10.1.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="/js/kindeditor-4.1.10/kindeditor-all-min.js"></script>
    <script type="text/javascript" charset="utf-8" src="/js/kindeditor-4.1.10/lang/zh_CN.js"></script>
</head>
<body>
    <form action="/news/addNew.html" method="post">
        新闻分类:<select name="catetory.catetoryId">
                    <option value="">请选择</option>
                    <c:forEach items="${catetories}" var="catetory">
                        <option value="${catetory.catetoryId}">${catetory.catetoryName}</option>
                    </c:forEach>
                  </select><br>
        作者:<input type="text" name="author" value="${sessionScope.USER_SESSION.userName}"><br>
        标题:<input type="text" name="title"><br>
        内容:<textarea style="width:800px;height:300px;visibility:hidden;" name="content" id="editor"></textarea><br>
        <input type="submit" value="发布">&nbsp;&nbsp;&nbsp;&nbsp;<input type="reset" value="取消">
    </form>
</body>

<script type="text/javascript">
    var kingEditorParams = {
        filePostName: "file",//指定上传文件参数名称
        //指定上传文件请求的url。
        uploadJson: '/news/upload.html?height=500&width=300',
        dir: "image"//上传类型，分别为image、flash、media、file,
    }
    var editor;
    $(function () {
        editor = KindEditor.create($("#editor"), kingEditorParams);
    });
</script>

</html>
