package com.service;

import com.pojo.User;

/**
 * @Houxudong
 **/
public interface UserService {
    void doRegist(User user);
    User getUserByName(String userName);
    User doLogin(User user);
    void doUpdateUser(User user);
    User getUserNById(Integer userId);
}
