package com.dao;

import com.pojo.User;

/**
 * @Houxudong
 **/
public interface UserDao {
    void addUser(User user);
    User getUserByName(String userName);
    void uploadUser(User user);
    User getUserById(Integer userId);
}
