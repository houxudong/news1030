package com.controller;

import com.github.pagehelper.PageInfo;
import com.pojo.Catetory;
import com.pojo.DateNews;
import com.pojo.News;
import com.service.CatetoryService;
import com.service.NewsService;
import com.util.ImageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Houxudong
 **/
@Controller
@RequestMapping("/news")
public class NewsController {
    @Autowired
    private NewsService newsService;
    @Autowired
    private CatetoryService catetoryService;

//    @InitBinder
//    public void initBinder(ServletRequestDataBinder binder) {
//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//        binder.registerCustomEditor(Date.class,
//                new CustomDateEditor(dateFormat, true));
//    }


    @RequestMapping("/addNew.html")
    public String addnew(News news){
        newsService.addNew(news);
        return "redirect:/news/select.html";
    }
    @RequestMapping("/getAllNews.html")
    public ModelAndView getAllnew(@RequestParam(defaultValue = "1") Integer pageNo, HttpSession session){
        List<Catetory> catetories = catetoryService.getAllCatetory();
        session.setAttribute("catetories",catetories);
        try {
            PageInfo<News> pageInfo = newsService.getNewsList(pageNo);
            return new ModelAndView("news_list","page",pageInfo);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }


    @RequestMapping(value = "/upload.html")//上传图片在页面上指定宽和高，缩放图片
    @ResponseBody
    public Map<String, Object> editorUpload(@RequestParam("file") MultipartFile file, Integer width, Integer height) {
        return fileUpload(file, width, height);
    }

    public Map<String, Object> fileUpload(MultipartFile file, Integer width, Integer height) {
        String extName = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
        String fileName = System.currentTimeMillis() + extName;
        try {
            File tempFile = new File(fileName);
            file.transferTo(tempFile);
            ImageUtil.imgThumb(tempFile, "E:/upload/" + fileName, width, height, 1.0f);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Map<String, Object> result = new HashMap<>();
        result.put("error", 0);
        result.put("url", "http://localhost:8080/upload/" + fileName);
        return result;
    }

    @RequestMapping("/getNewsInfo.html")
    public ModelAndView getNewsInfo(Integer newsId){
        try {
            News news = newsService.getNewInfoById(newsId);
            System.out.println("内容:"+news.getContent());
            return new ModelAndView("news_info","newsInfo",news);
        }catch (Exception e){
            e.printStackTrace();
        }
       return null;
    }
    @RequestMapping("/del.html")
    @ResponseBody
    public Map<String,Object> del(Integer newId){
        Map<String,Object> result = new HashMap<>();
        System.out.println("xxx");
        try {
            newsService.delNew(newId);
            result.put("status",true);
            result.put("msg","删除数据成功");
            return result;
        }catch (Exception e){
            e.printStackTrace();
            result.put("status",false);
            result.put("msg","删除数据失败");
            return result;
        }
    }
    @RequestMapping("/select.html")
    public ModelAndView select(@RequestParam(defaultValue = "1")Integer pageNo, DateNews news,HttpSession session) {
        List<Catetory> catetories = catetoryService.getAllCatetory();
        session.setAttribute("catetories",catetories);
        PageInfo<News> pageInfo = newsService.getNewsBycri(news, pageNo);
        ModelAndView modelAndView = new ModelAndView();
       // modelAndView.setViewName("news_list");
        modelAndView.setViewName("newslistBycri");
        modelAndView.addObject("page", pageInfo);
        modelAndView.addObject("newsCri", news);
        return modelAndView;

    }
}
