<%@ page pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>新闻列表</title>
    <script type="text/javascript" src="/js/jquery-1.10.1.min.js"></script>
    <style type="text/css">
        input{
            width: 80px;
        }
    </style>
</head>
<body>
<form action="/news/select.html" method="post">
    标题:<input  type="text" name="title">&nbsp;
    作者:<input type="text" name="author">&nbsp;
    内容:<input type="text" name="content">&nbsp;
    类型:<select name="catetory.catetoryId">
    <option value="">请选择</option>
    <c:forEach items="${sessionScope.catetories}" var="catetory">
        <option value="${catetory.catetoryId}">${catetory.catetoryName}</option>
    </c:forEach>
</select>&nbsp;
    发布时间:<input type="date" name="date1" style="width:130px;">-
    <input type="date" name="date2" style="width:130px;">&nbsp;
    <input type="submit" value="查询">
</form>
    <div style="margin-left: 30px; width: 500px;">
        <a href="/user/index.html">返回首页</a>

        <table border="1">
            <tr>
                <th>新闻编号</th>
                <th>标题</th>
                <th>作者</th>
                <th>类别</th>
                <th>发布时间</th>
                <th colspan="2">操作</th>
            </tr>
            <c:forEach items="${page.list}" var="news">
                <tr>
                    <td>${news.newId}</td>
                    <td>${news.title}</td>
                    <td>${news.author}</td>
                    <td>${news.catetory.catetoryName}</td>
                    <td><fmt:formatDate value="${news.addTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
                    <td><a href="/news/getNewsInfo.html?newsId=${news.newId}">详情</a></td>
                    <td><a href="javascript:del('${news.newId}')">删除</a></td>
                </tr>
            </c:forEach>
        </table>

        <div>
            当前第【${page.pageNum}】
            <a href="/news/select.html?pageNo=${page.prePage}&$title=${newsCri.title}&author=${newsCri.author}&content=${newsCri.content}&catetory.catetoryId=${newsCri.catetory.catetoryId}&date1=${newsCri.date1}&date2=${newsCri.date2}">上一页</a>
            <c:forEach begin="${page.navigateFirstPage}" end="${page.navigateLastPage}" var="pn">
                <a href="/news/select.html?pageNo=${pn}&$title=${newsCri.title}&author=${newsCri.author}&content=${newsCri.content}&catetory.catetoryId=${newsCri.catetory.catetoryId}&date1=${newsCri.date1}&date2=${newsCri.date2}"
                   class="pageNav  ${page.pageNum==pn?'hover':''}">${pn}</a>
            </c:forEach>

            <a href="/news/select.html?pageNo=${page.nextPage}&$title=${newsCri.title}&author=${newsCri.author}&content=${newsCri.content}&catetory.catetoryId=${newsCri.catetory.catetoryId}&date1=${newsCri.date1}&date2=${newsCri.date2}">下一页</a>
            共【${page.pages}】页
        </div>

    </div>

</body>
<script type="text/javascript">
    function del(obj) {
        $.ajax({
            url:"/news/del.html",
            data:"newId="+obj,
            success:function (data) {
                if(data.status==true){
                    alert(data.msg);
                    window.location.href='/news/getAllNews.html';
                }
            }
        });
    }
</script>
</html>
