package com.controller;

import com.pojo.Catetory;
import com.service.CatetoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @Houxudong
 **/
@Controller
@RequestMapping("/catetory")
public class CatetoryController {
    @Autowired
    private CatetoryService catetoryService;
    @RequestMapping("/getAllCatetory.html")
    public ModelAndView getAll(HttpSession session){
        List<Catetory> catetories = catetoryService.getAllCatetory();
        session.setAttribute("catetories",catetories);
        return new ModelAndView("addnew","catetories",catetories);
    }
}
