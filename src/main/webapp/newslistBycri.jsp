<%@ page pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>条件查询</title>
    <script type="text/javascript" src="/js/jquery-1.10.1.min.js"></script>
    <style type="text/css">
        input{
            width: 80px;
        }
    </style>
</head>
<body>
<form action="/news/select.html" method="post" id="searchForm">
    <input type="hidden" name="pageNo" id="pageNum">
    标题:<input  type="text" name="title" value="${param.title}">&nbsp;
    作者:<input type="text" name="author" value="${newsCri.author}">&nbsp;
    内容:<input type="text" name="content" value="${param.content}">&nbsp;
    类型:<select name="catetory.catetoryId">
    <option value="">请选择</option>
    <c:forEach items="${sessionScope.catetories}" var="catetory">
        <option value="${catetory.catetoryId}" ${newsCri.catetory.catetoryId==catetory.catetoryId?"selected":""}>${catetory.catetoryName}</option>
    </c:forEach>
</select>&nbsp;
    发布时间:<input type="date" name="date1" style="width:130px;" value="${param.date1}">-
    <input type="date" name="date2" style="width:130px;" value="${param.date2}">&nbsp;
    <input type="submit" value="查询">
</form>
<div style="margin-left: 30px; width: 500px;">
    <a href="/user/index.html">返回首页</a>

    <table border="1">
        <tr>
            <th>新闻编号</th>
            <th>标题</th>
            <th>作者</th>
            <th>类别</th>
            <th>发布时间</th>
            <th colspan="2">操作</th>
        </tr>
        <c:forEach items="${page.list}" var="news">
            <tr>
                <td>${news.newId}</td>
                <td>${news.title}</td>
                <td>${news.author}</td>
                <td>${news.catetory.catetoryName}</td>
                <td><fmt:formatDate value="${news.addTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
                <td><a href="/news/getNewsInfo.html?newsId=${news.newId}">详情</a></td>
                <td><a href="javascript:del('${news.newId}')">删除</a></td>
            </tr>
        </c:forEach>
    </table>

    <div style="margin-left: 150px;">
        <a href="javascript:page('${page.prePage}')">上一页</a>
        <c:forEach begin="${page.navigateFirstPage}" end="${page.navigateLastPage}" var="pn">
            <c:choose>
                <c:when test="${page.pageNum==pn}">
                    ${pn}
                </c:when>
                <c:otherwise>
                    <a href="#" onclick="page(${pn})"
                       class="pageNav  ${page.pageNum==pn?'hover':''}">${pn}</a>
                </c:otherwise>
            </c:choose>
        </c:forEach>
        <a href="javascript:page('${page.nextPage}')">下一页</a>
    </div>

</div>

</body>
<script type="text/javascript">
    function page(pageNum) {
        if (pageNum == null || isNaN(pageNum) || pageNum < 0) {
            pageNum = 1;
        }
        $("#pageNum").val(pageNum);
        $("#searchForm").submit();
    }
</script>
<script type="text/javascript">
    function del(obj) {
        $.ajax({
            url:"/news/del.html",
            data:"newId="+obj,
            success:function (data) {
                if(data.status==true){
                    alert(data.msg);
                    window.location.href='/news/getAllNews.html';
                }
            }
        });
    }
</script>
</html>
