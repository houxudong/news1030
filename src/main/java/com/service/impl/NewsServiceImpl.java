package com.service.impl;

import com.dao.NewsDao;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pojo.News;
import com.service.NewsService;
import com.sun.org.apache.bcel.internal.generic.NEW;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Houxudong
 **/
@Service
public class NewsServiceImpl implements NewsService{
    @Autowired
    private NewsDao newsDao;
    @Override
    public void addNew(News news) {
        newsDao.addNew(news);
    }

    @Override
    public PageInfo<News> getNewsList(Integer pageNo) {
        PageHelper.startPage(pageNo,5);
        List<News> list =  newsDao.getNewsList();
        PageInfo<News> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    @Override
    public News getNewInfoById(Integer newId) {
        return newsDao.getNewById(newId);
    }

    @Override
    public void delNew(Integer newId) {
        newsDao.delNew(newId);
    }

    @Override
    public PageInfo<News> getNewsBycri(News news,Integer pageNo) {
        PageHelper.startPage(pageNo,5,"addTime desc");
        List<News> list = newsDao.getList(news);
        PageInfo<News> pageInfo = new PageInfo<>(list,3);
        return pageInfo;
    }
}
