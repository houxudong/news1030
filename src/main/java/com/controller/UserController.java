package com.controller;

import com.pojo.User;
import com.service.UserService;
import com.util.Constants;
import com.util.ImageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * @Houxudong
 **/
@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;
    @RequestMapping("/index.html")
    public String index(){
        return "index";
    }

    @RequestMapping("/doRegist.html")
    public String regist(User user){
        userService.doRegist(user);
        return "login";
    }
    @RequestMapping("/check.html")
    @ResponseBody
    public Map<String,Object> check(String userName){
        User user = userService.getUserByName(userName);
        Map<String,Object> result = new HashMap<>();
        if(user!=null){
            result.put("status",false);
            result.put("msg","用户名已被占用");
        }else{
            result.put("status",true);
            result.put("msg","用户名可以使用");
        }
        return result;
    }
    @RequestMapping("/doLogin.html")
    public ModelAndView login(User user, HttpSession session){

        try{
            User newUser = userService.doLogin(user);
            session.setAttribute(Constants.USER_SESSION,newUser);
            return new ModelAndView("index");
        }catch (Exception e){
            e.printStackTrace();
            return new ModelAndView("login","err","用户名或密码错误");
        }
    }
    @RequestMapping("/doLoginout.html")
    public String loginout(HttpSession session){
        session.invalidate();
        return "login";
    }

    @RequestMapping(value = "/upload.html")
    @ResponseBody
    public String fileUpload1(@RequestParam("file") MultipartFile file) {
        String extName = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
        String fileName = System.currentTimeMillis() + extName;
        try {

            //将上传的文件存在E:/upload/下
//            FileUtils.copyInputStreamToFile(file.getInputStream(), new File("E:/upload/",
//                    fileName));
            File newFile = new File("E:/upload/", fileName);
            //通过CommonsMultipartFile的方法直接写文件
            file.transferTo(newFile);

            ImageUtil.imgThumb(newFile, "E:/upload/"+ fileName, 500, 500, 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //上传成功返回原来页面
        return  fileName;
    }
    @RequestMapping("/cut.html")
    @ResponseBody
    public String cutImg(String fileName, int x1, int y1, int w, int h) throws Exception {
        ImageUtil.imgSourceRegion("e:/upload/" + fileName, "e:/upload/" + fileName, x1, y1, w, h, 100, 100, true);
        return fileName;
    }

    @RequestMapping("/doUpload.html")
    public String doUpload(User user,HttpSession session){
        Integer userId = user.getUserId();
        System.out.println(user.getUserId());
        System.out.println(user.getUserName());
        System.out.println("头像:"+user.getAvator());
        userService.doUpdateUser(user);
        return "redirect:/user/go_selfInfo.html?userId="+userId;
    }
    @RequestMapping("/go_selfInfo.html")
    public ModelAndView go_selfinfo(Integer userId,HttpSession session){
        User user = userService.getUserNById(userId);
        session.setAttribute(Constants.USER_SESSION,user);
        return new ModelAndView("selfInfo","user",user);
    }
}
