package com.dao;

import com.pojo.News;

import java.util.List;

/**
 * @Houxudong
 **/
public interface NewsDao {
    void addNew(News news);
    List<News> getNewsList();
    News getNewById(Integer newsId);
    void delNew(Integer newId);
    List<News> getList(News news);
}
