package com.pojo;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @Houxudong
 **/
public class News {
    private Integer newId;
    private String title;
    private String author;
    private String content;
    private Catetory catetory /*= new Catetory()*/;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date addTime;

    public Integer getNewId() {
        return newId;
    }

    public void setNewId(Integer newId) {
        this.newId = newId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Catetory getCatetory() {
        return catetory;
    }

    public void setCatetory(Catetory catetory) {
        this.catetory = catetory;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }
}
