package com.service;

import com.github.pagehelper.PageInfo;
import com.pojo.News;

/**
 * @Houxudong
 **/
public interface NewsService {
    void addNew(News news);
    PageInfo<News> getNewsList(Integer pageNo);
    News getNewInfoById(Integer newId);
    void delNew(Integer newId);
    PageInfo<News> getNewsBycri(News news,Integer pageNo);
}
