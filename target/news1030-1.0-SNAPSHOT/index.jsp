<%@ page pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<body>
<h2>欢迎您 ${sessionScope.USER_SESSION.userName}</h2>
<c:choose>
    <c:when test="${sessionScope.USER_SESSION==null}">
        <a href="/login.jsp">登录</a><br>
        <a href="/regist.jsp">注册</a><br>
    </c:when>
    <c:otherwise>
        <a href="/updateselfInfo.jsp">修改个人信息</a><br>
        <a href="/user/doLoginout.html">退出登录</a><br>
        <a href="/catetory/getAllCatetory.html">发布新闻</a><br>
    </c:otherwise>
</c:choose>
<a href="/news/select.html">新闻列表</a><br>
</body>
</html>
